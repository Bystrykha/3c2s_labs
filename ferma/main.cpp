#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <cmath>
#include <vector>

using namespace std;

bool validCheck(const string& inputString, int &n){
    n = stoi(inputString);
    if (n % 2 == 1){
        for(int i = 3; i <= sqrt(n); i += 2){
            if (n % i == 0){
                return true;
            }
        }
    }
    return false;
}

vector <int> quadraticResidue(int mod){
    vector <int> resVec;
    for (int i = 1; i < mod; ++i){
        int a = (i * i) % mod, b = a - mod;
        if ( !(find(resVec.begin(), resVec.end(), a) != resVec.end()) ){
            resVec.push_back(a);
        }
        if ( !(find(resVec.begin(), resVec.end(), b) != resVec.end()) ){
            resVec.push_back(b);
        }
    }
    return resVec;
}


pair<int, int> ferma(int &n) {
    int x = (int) sqrt(n), z, y, a, b;
    if (x * x == n) return {x, x};
    int matrix [3][5] = {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};
    int j = 0;
    vector <int> resVec = quadraticResidue(3);
    for (int i = 0; i < 3; ++i){
        if (((j * j - n) % 3 == 0) ||
                find(resVec.begin(), resVec.end(), (j * j - n) % 3) != resVec.end()){
            matrix[0][i] = 1;
        } else matrix[0][i] = 0;
        ++j;
    }
    j = 0;
    resVec = quadraticResidue(4);
    for (int i = 0; i < 4; ++i){
        if (((j * j - n) % 4 == 0) ||
                find(resVec.begin(), resVec.end(), (j * j - n) % 4) != resVec.end()){
            matrix[1][i] = 1;
        } else matrix[1][i] = 0;
        ++j;
    }
    j = 0;
    resVec = quadraticResidue(5);
    for (int i = 0; i < 5; ++i){
        if (((j * j - n) % 5 == 0) ||
                find(resVec.begin(), resVec.end(), (j * j - n) % 5) != resVec.end()){
            matrix[2][i] = 1;
        } else matrix[2][i] = 0;
        ++j;
    }
    ++x;
    int r1 = x % 3, r2 = x % 4, r3 = x % 5;
    while (true){
        if ((matrix[0][r1] == 1) && (matrix[1][r2] == 1) && (matrix[2][r3] == 1)){
            z = x * x - n;
            y = int(sqrt(z));
            if(y * y == z){
                a = x + y;
                b = x - y;
                return {a, b};
            }
        }
        ++x;
        r1 = (r1 + 1) % 3;
        r2 = (r2 + 1) % 4;
        r3 = (r3 + 1) % 5;
    }
}

int getRes(string &dir){
    ifstream file(dir);
    string inputString;
    regex patternString("^[0-9]+$");
    if(file.is_open()){
        ofstream out;
        out.open("output.txt", ios::app);
        int n;
        while(getline(file, inputString)) {
            if ((regex_match(inputString, patternString)) &&
                validCheck(inputString, n)){
                auto [a, b] = ferma(n);
                out << a << " " << b << "\n";
            }
        }
        file.close();
        out.close();
        return 1;
    }
    else return 0;
}

int main() {
    string directory = "input.txt";
    int res = getRes(directory);
    return res;
}
