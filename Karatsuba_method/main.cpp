#include <iostream>
#include <fstream>
#include <string>
#include <regex>

int numbers[3];
std::string one_str_from_file;
std::regex pattern_str("[0-9]+ [0-9]+");
unsigned long long u1 = 0, u0 = 0, v1 = 0, v0 = 0, n = 0;

void katsuba_method() {
    unsigned long long A = u1 * v1;
    unsigned long long C = (u1 + u0) * (v1 + v0);
    unsigned long long B = u0 * v0;
    int bn = 10;
    for(int i = 0; i < n; i++) bn *= 10;
    unsigned long long  res1 = A * bn * bn;
    unsigned long long res2 = (C - A - B) * bn;
    unsigned long long res = res1 + res2 + B;
    u1 = 0, u0 = 0, v1 = 0, v0 = 0, n = 0;
    std::ofstream out;
    out.open("output.txt", std::ios::app);
    out << res << "\n";
}

void get_numbers() {
    std::string numb_1, numb_2;
    int a = 0;
    for(char i : one_str_from_file){
        if(a == 0 && i != ' ') numb_1 += i;
        if(a == 1) numb_2 += i;
        if(i == ' ') a = 1;
    }
    while (numb_1.size() != numb_2.size()){
        if(numb_1.size() > numb_2.size()) numb_2.insert(0, "0");
        if(numb_1.size() < numb_2.size()) numb_1.insert(0, "0");
    }
    if(numb_1.size() % 2 != 0) numb_1.insert(0, "0");
    if(numb_2.size() % 2 != 0) numb_2.insert(0, "0");
    int mid = numb_1.size() / 2;
    n = mid - 1;
    for(int i = 0; i < numb_1.size(); i++) {
        if (i <= mid - 1) {
            u1 = u1 * 10 + (numb_1[i] - 48);
            v1 = v1 * 10 + (numb_2[i] - 48);
        } else {
            u0 = u0 * 10 + (numb_1[i] - 48);
            v0 = v0 * 10 + (numb_2[i] - 48);
        }
    }
}

int numb_define(std::string dir){
    std::ifstream file(dir);
    if(file.is_open()){
        while(getline(file, one_str_from_file)) {
            if (std::regex_match(one_str_from_file, pattern_str)) {
                get_numbers();
                katsuba_method();
            }
        }
        return 1;
    }
    else return 0;
}

int main(){
    std::string directory = "input.txt";
    int res = numb_define(directory);
    return res;
}
