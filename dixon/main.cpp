/// для подробного вывода, раскомментируйте фрагменты кода

#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <utility>
#include <vector>
#include <cmath>
#include <cstdlib>

using namespace std;

class ABE{

public:
    int aMean = 0;
    int bMean = 0;
    vector <int> eMeans = {};

    ABE(int aMean, int bMean, vector <int> eMeans){
        this->aMean = aMean;
        this->bMean = bMean;
        this->eMeans = std::move(eMeans);
    };
};

int NOD (int x, int y){
    if (x < 0) x *= -1;
    while (x != y){
        if (x > y) x -= y;
        else y -= x;
    }
    return x;
}

int stringToNumb(const string& strNumb){
    int number = 0;
    for (char symbol : strNumb){
        number = number * 10 + (symbol - '0');
    }
    return number;
}

void getNumbers(string &inputString, int & number, int & baseLen) {
    string strNumb;
    for (char symbol: inputString){
        if (symbol == ' ') {
            number = stringToNumb(strNumb);
            strNumb = "";
        }
        else{
            strNumb += symbol;
        }
    }
    baseLen = stringToNumb(strNumb);
}

auto primeTest(int n){
    for(int a = 2; a <= sqrt(n); a++){
        if (n % a == 0){
            return false;
        }
    }
    return true;
}

bool validTest(int &number){
    while (number % 2 == 0) number /= 2;
    if (!primeTest(number)) return true;
    else return false;
}

void fBCreate(int n, vector <int> &factorBase, int k){
    if (n >= 2) factorBase.push_back(2);
    for (int i = 3; i <= n; ++i){
        if (primeTest(i)) factorBase.push_back(i);
        if (factorBase.size() == k) break;
    }
}

void abeCreate(int n, vector <ABE> &abe, int t, vector <int> &S){
    for (int j = 0; j < t; ++j){
        int a = int(rand()) % (2 * n) + 10;
        int b = a * a % n;
        if(b == 0){
            j -= 1;
            continue;
        }
        vector <int> e;
        for (int i = 0; i < S.size(); ++i) e.push_back(0);
        bool zeroVector = true;
        int bCopy = b;
        for (int i = 0; i < S.size(); ++i) {
            while (bCopy % S[i] == 0){
                bCopy /= S[i];
                e[i] += 1;
            }
        }
        for (int &i : e) {
            if (i == 1) zeroVector = false;
        }
        if (!zeroVector && bCopy == 1){
            ABE el = ABE(a, b, e);
            abe.push_back(el);
            continue;
        } else j -= 1;
   }
}

void matrixCreate(vector <vector <int>> &V, vector <ABE> &abe){
    for (auto & i : abe){
        vector <int> v;
        for (int j : i.eMeans)v.push_back(j % 2);
        V.push_back(v);
    }
}

bool cVectorCreate(int c, vector <int> &C, vector <vector <int>> &V){
    vector <int> v;
    while (c != 0){
        v.push_back(c % 2);
        c /= 2;
    }
    if (v.size() < C.size()) while (v.size() < C.size()) v.push_back(0);
    for (int i = 0; i < v.size(); ++i) C[i] = v[v.size() - 1 - i];
    for (int i = 0; i < V[0].size(); ++i){
        int sum = 0;
        for (int j = 0; j < V.size(); ++j){
            sum += C[j] * V[j][i];
        }
        if (sum % 2 == 1) return false;
    }
    return true;
}


vector <int> dixonAlgorithm(int n, int k){
    vector <int> resVector, S;
    vector <ABE> abe;
    fBCreate(n, S, k);
//    cout << "S = ";
//    for (int & i : S){
//        cout << i << " ";
//    }
//    cout << endl;
    abeCreate(n, abe, k + 1, S);
    while (true) {
//        for (auto & i : abe){
//            cout << "a = " << i.aMean << "; b = " << i.bMean << "; e = ";
//            for (int &j : i.eMeans){
//                cout << j << ", ";
//            }
//            cout << endl;
//        }
        vector <vector <int>> V;
        matrixCreate(V, abe);
//        for (auto &i : V) {
//            cout << "\t";
//            for (int &j : i) {
//                cout << j << " ";
//            }
//            cout << endl;
//        }
        vector <int> C;
        for (int i = 0; i < V.size(); ++i) C.push_back(0);
        for (int c = 1; c <= int(pow(2, V.size())) - 1; ++c) {
            if (cVectorCreate(c, C, V)) {
//                cout << "vector c: ";
//                for (int &i : C) cout << i << " ";
//                cout << endl;
                vector<int> I;
                for (int i = 0; i < C.size(); ++i) if (C[i] == 1) I.push_back(i);
//                cout << "vector I: ";
//                for (int &i : I) cout << i << " ";
//                cout << endl;
                int x = 1, y = 1;
//                cout << "x = ";
                for (int &i : I) {
//                    cout << abe[i].aMean << "*";
                    x *= abe[i].aMean;
                }
//                cout << " = " << x << endl;
                x %= n;
//                cout << "x mod(n) = " << x << endl;
//                cout << "y = ";
                for (int i = 0; i < S.size(); ++i) {
//                    cout << S[i] << "^(";
                    int deg = 0;
                    for (auto &j : I) {
                        deg += abe[j].eMeans[i];
//                        cout << deg << " + ";
                    }
//                    cout << ")/2 *";
                    deg /= 2;
                    y *= pow(S[i], deg);
                }
//                cout << " = " << y << endl;
                if ((x % n) == (y % n) || (x + y) % n == 0) {
//                    cout << endl;
                    continue;
                } else {
//                    cout << "RESULT IS: " << NOD(x - y, n) << " " << NOD(x + y, n) << endl;
                    resVector.push_back(NOD(x - y, n));
                    resVector.push_back(NOD(x + y, n));
                    return resVector;
                }
            } else continue;
        }
//        cout << endl << "t++" << endl << endl;
        vector <ABE> addABE;
        abeCreate(n, addABE, 1, S);
        abe.push_back(addABE[0]);
    }
}

int getRes(string &dir){
    ifstream file(dir);
    string inputString;
    regex patternString("[0-9]+ [0-9]+");
    string numberString, baseLenString;
    if(file.is_open()){
        ofstream out;
        out.open("output.txt", ios::app);
        while(getline(file, inputString)) {
            int number = 0, baseLen = 0;
            if (regex_match(inputString, patternString)) {
                getNumbers(inputString, number, baseLen);
                if (validTest(number)){
                    vector <int> res;
                    res = dixonAlgorithm(number, baseLen);
                    for(int i: res){
                        out << i << " ";
                    }
                    out << "\n";
                } else continue;
            }
        }
        file.close();
        out.close();
        return 1;
    }
    else return 0;
}

int main() {
    srand(static_cast<unsigned int>(time(0)));
    string directory = "input.txt";
    int res = getRes(directory);
    return res;
}
