#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include <cmath>

int s2n(const std::string& strNumb){
    int number = 0;
    for (char symbol : strNumb){
        number = number * 10 + (symbol - '0');
    }
    return number;
}

void getNumbers(std::string &inputString, int & number, int & border) {
    std::string strNumb;
    for (char symbol: inputString){
        if (symbol == ' ') {
            number = s2n(strNumb);
            strNumb = "";
        }
        else{
            strNumb += symbol;
        }
    }
    border = s2n(strNumb);
}

auto primeTest(int n){
    for(int a = 2; a <= sqrt(n); a++){
        if (n % a == 0){
            return false;
        }
    }
    return true;
}

std::vector <int> trailDivision(int number, int border, int & remainder, bool & flag){
    std::vector <int> resVector;
    if(number == 1){
        remainder = 1;
        flag = false;
        return resVector;
    }
    while (number % 2 == 0){
        number /= 2;
        resVector.push_back(2);
    }
    int d = 3;
    while ((number > 1) && (d <= border)) {
        if (number % d == 0) {
            number /= d;
            resVector.push_back(d);
        }
        else d++;
    }
        remainder = number;
    if(number == 1) {
       // remainder = number;
        flag = false;
        return resVector;
    }
    if(primeTest(number)) {
        resVector.push_back(number);
        remainder = 1;
        flag = false;
        return resVector;
    }
        flag = true;
    return resVector;
}

int getRes(std::string &dir){
    std::ifstream file(dir);
    std::string inputString;
    std::regex patternString("[1-9]([0-9]+)? [1-9]([0-9]+)?");
    std::string numberString, borderString;
    if(file.is_open()){
        std::ofstream out;
        out.open("output.txt", std::ios::app);
        while(getline(file, inputString)) {
            int number = 0, border = 0, remainder = 0;
            bool flag = false;
            if (std::regex_match(inputString, patternString)) {
                getNumbers(inputString, number, border);
                std::vector <int> resVector = trailDivision(number, border, remainder, flag);
                if(resVector.empty()){
                    out << remainder << " " << flag << "\n";
                }
                else{
                    for(int i: resVector){
                        out << i << " ";
                    }
                    out << remainder << " " << flag << "\n";
                }
            }
        }
        file.close();
        out.close();
        return 1;
    }
    else return 0;
}

int main() {
 std::string directory = "input.txt";
   int res = getRes(directory);
    return res;
}
