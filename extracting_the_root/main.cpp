#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <regex>

std::string one_str_from_file;
std::regex pattern_str("[0-9]+ [1-9]+");
unsigned long long a, n;

void extracting_the_root(){
    unsigned long long a1 = a, a0 = 0;
    do{
        a0 = a1;
        unsigned long long k = 1;
        for(int i = 0; i < n - 1; i++) k = k * a0;
        a1 = (unsigned long long)((unsigned long long)a/k + (n - 1) * a0) / n;
    }while(a0 > a1);
    std::ofstream out;
    out.open("output.txt", std::ios::app);
    out << a0 << "\n";
    a = 0, n = 0;
}

int numb_define(const std::string& dir){
    std::ifstream file;
    file.open(dir);
    if(file.is_open()){
        while(getline(file, one_str_from_file)) {       //считывание из файла
            if (std::regex_match(one_str_from_file, pattern_str)) {     // сравнение с регулярным выражением
                for (int i = 0, j = 0; j < one_str_from_file.length(); j++) {     // разбиение валидной строки на числа
                    if (one_str_from_file[j] >= '0' && one_str_from_file[j] <= '9') {
                        if(i == 0) a = a * 10 + (one_str_from_file[j] - 48);
                        if(i == 1) n = n * 10 + (one_str_from_file[j] - 48);
                    }
                    else i++;
                }
                extracting_the_root();   // подсчет справа на лево
            }
        }
    }
    else return 0;
    return 1;
}

int main(){
    std::string directory = "input.txt";
    int res = numb_define(directory);
    return res;
}
