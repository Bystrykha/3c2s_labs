#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <cmath>

bool validCheck(const std::string& inputString, int &n, int &d){
    n = std::stoi(inputString);
    while(n % 2 == 0) {
        n = n / 2;
    }
    if (n <= 5 ) return false;
    int a = 0;
    for (int i = 3; i <= sqrt(n); i += 2){
        if (n % i == 0){
            a = 1;
            break;
        }
    }
    if (a == 0) return false;
    d = 2 * int(cbrt(n)) + 1;
    return true;
}

int olway(int &n, int &d){
    int r, r1 = n % d, r2 = n % (d - 2),
    q = 4 * (int(n / (d - 2)) - int(n / d)), s = int(sqrt(n));
    while (true){
        d += 2;
        if (d > s) return 0;
        r = 2 * r1 - r2 + q;
        if(r < 0) {
            r += d;
            q += 4;


        }
        while (r >= d){
            r -= d;
            q -= 4;
        }
        if (r == 0) return d;
        else{
            r2 = r1;
            r1 = r;
        }
    }
}

int getRes(std::string &dir) {
    std::ifstream file(dir);
    std::string inputString;
    std::regex patternString("^[0-9]+$");
    if (file.is_open()) {
        std::ofstream out;
        out.open("output.txt", std::ios::app);
        int n, d;
        while (getline(file, inputString)) {
            if (std::regex_match(inputString, patternString) &&
                    validCheck(inputString, n, d)) {
                    int a = olway(n, d);
                    if (a != 0) out << a << "\n";
            }
        }
        file.close();
        out.close();
        return 1;
    }
    return 0;
}

int main() {
 std::string directory = "input.txt";
   int res = getRes(directory);
   return res;
}
