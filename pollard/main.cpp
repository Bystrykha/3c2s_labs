#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include <cmath>

using namespace std;

int NOD (int x, int y){
    if (x < 0) x *= -1;
    while (x != y){
        if (x > y) x -= y;
        else y -= x;
    }
    return x;
}

int stringToNumber(const string& strNumb){
    int number = 0;
    for (char symbol : strNumb){
        number = number * 10 + (symbol - '0');
    }
    return number;
}

void getNumbers(string &inputString, int & number, int & border) {
    string strNumb;
    for (char symbol: inputString){
        if (symbol == ' ') {
            number = stringToNumber(strNumb);
            strNumb = "";
        }
        else{
            strNumb += symbol;
        }
    }
    border = stringToNumber(strNumb);
}

auto primeTest(int n){
    for(int a = 2; a <= sqrt(n); a++){
        if (n % a == 0){
            return false;
        }
    }
    return true;
}

bool validation(int &n, int &a){
    if(n > 0 && primeTest(n) && a > 0 && a < n) return true;
    return false;
}

int gropeBase(int &n, int &a){
    int p = n - 1;
    vector<int> Q;
    for(int i = 2; i < n / 2 + 1; ++i){
        if (primeTest(i) && p % i == 0){
            while (p % i == 0) p /= i;
            Q.push_back(i);
        }
    }
    for (int j = 2; j < n; ++j){
        bool k = true;
        for (int i : Q){
            unsigned int b = pow(j, (n-1) / i);
            if (b % n == 1){
                k = false;
                break;
            }
        }
        if (k) return j;
    }
    return -1;
}

void F(int &x, int &y, int &z, int a, int n, int g){
    if (x % 3 == 1){
        x = (a * x) % n;
        y = (y + 1) % (n - 1);
    }else if (x % 3 == 2) {
        x = (x * x) % n;
        y = (2 * y) % (n - 1);
        z = (2 * z) % (n - 1);
    }else if (x % 3 == 0) {
        x = (g * x) % n;
        z = (z + 1) % (n - 1);
    }
}

int pollard(int &g, int &n, int &a){
    int x1 = 1, x2 = 1, y1 = 0, y2 = 0, z1 = 0, z2 = 0;
    while (true){
        F(x1, y1, z1, a, n, g);
        F(x2, y2, z2, a, n, g);
        F(x2, y2, z2, a, n, g);
        if(x1 == x2){
            if(y1 == y2){
                return -1;
            } else{
                vector <int> B;
                int d = NOD((n - 1 + y1 - y2) % (n - 1), n - 1);
                for(int i = 1; i < n; ++i){
                    int l = ((n-1 + y1 - y2) * i) % (n-1);
                    int r = (n-1 + z2 - z1) % (n-1);
                    if(l == r) B.push_back(i);
                }
                for (int i = 0; i < d; ++i){
                    int k = 1;
                    for (int j = 1; j <= B[i]; ++j) k = k * g % n;
                    if (k == a) return B[i];
                }
            }

        }
    }
}

int getRes(string &dir){
    ifstream file(dir);
    string inputString;
    regex patternString("[0-9]+ [0-9]+");
    string numberString, borderString;
    if(file.is_open()){
        ofstream out;
        out.open("output.txt", ios::app);
        while(getline(file, inputString)) {
            int n = 0, a = 0;
            if (regex_match(inputString, patternString)) {
                getNumbers(inputString, n, a);
                if(validation(n, a)){
                    int g = gropeBase(n, a);
                    if (g != -1){
                        int res = pollard(g, n, a);
                        if(res != -1){
                            out << res << "\n";
                        }
                    }
                } else continue;
            }
        }
        file.close();
        out.close();
        return 1;
    }
    else return 0;
}

int main() {
    string directory = "input.txt";
    int res = getRes(directory);
    return res;
}
