#include <iostream>
#include <fstream>
#include <string>
#include <regex>

using namespace std;

int numbers[3];
std::string one_str_from_file;
std::regex pattern_str("[0-9]+ [0-9]+");
unsigned long long m, x, r;

void get_numbers() {
    std::string numb_1, numb_2;
    int a = 0;
    for(char i : one_str_from_file){
        if(a == 0 && i != ' ') numb_1 += i;
        if(a == 1) numb_2 += i;
        if(i == ' ') a = 1;
    }
    for(char i : numb_1) x = x * 10 + i - 48;
    for(char i : numb_2) m = m * 10 + i - 48;
}

void Out_to_file(){
    std::ofstream out;
    out.open("output.txt", std::ios::app);
    out << r << "\n";
    m = 0;
    x = 0;
    r = 0;
}

void Barret_method(){
    if(x == m){
        x = 0;
        m = 0;
        r = 0;
        return;
    }
    int k = 0, n = 0;
    unsigned long long max = m;
    while(max != 0){
        max >>= 2u;
        k++;
    }
    max = x;
    while (max != 0){
        max >>= 2u;
        n++;
    }
    if(n > 2 * k){
        x = 0;
        m = 0;
        r = 0;
        return;
    }
    unsigned long long z = (1ull << (4 * k)) / m;
    unsigned long long tx = x >> 2*(k-1);
    unsigned long long q = (tx * z)>>2*(k+1);
    unsigned long long mask = (1u<<(2*(k+1)))-1;
    unsigned long long r1 = (x & mask);
    unsigned long long r2 = ((q*m) & mask);
    if(r1 >= r2) r = r1-r2;
    else r = (1u<<2*(k+1)) + r1 - r2;
    while(r >= m){
        r -= m;
    }
    Out_to_file();
}

int numb_define(std::string dir){
    std::ifstream file(dir);
    if(file.is_open()){
        char c;
        while(getline(file, one_str_from_file)) {
            if (std::regex_match(one_str_from_file, pattern_str)) {
                get_numbers();
                if(x == m) Out_to_file();
                else Barret_method();
            }
        }
        return 1;
    }
    else return 0;
}

int main(){
    std::string directory = "input.txt";
    int res = numb_define(directory);
    return res;
}
