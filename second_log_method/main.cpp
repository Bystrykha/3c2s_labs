#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include <cmath>

using namespace std;

int NOD (int x, int y){
    if (x < 0) x *= -1;
    while (x != y){
        if (x > y) x -= y;
        else y -= x;
    }
    return x;
}

int stringToNumber(const string& strNumb){
    int number = 0;
    for (char symbol : strNumb){
        number = number * 10 + (symbol - '0');
    }
    return number;
}

void getNumbers(string &inputString, int & number, int & border) {
    string strNumb;
    for (char symbol: inputString){
        if (symbol == ' ') {
            number = stringToNumber(strNumb);
            strNumb = "";
        }
        else{
            strNumb += symbol;
        }
    }
    border = stringToNumber(strNumb);
}

int phi (int n) {
    int result = n;
    for (int i=2; i*i<=n; ++i)
        if (n % i == 0) {
            while (n % i == 0)
                n /= i;
            result -= result / i;
        }
    if (n > 1)
        result -= result / n;
    return result;
}

int pow(int x, int y, int n){
    while(y < 0){
        y += phi(n);
    }
    int k = 1;
    for (int i = 0; i < y; ++i){
        k *= x;
        k %= n;
    }
    return k;
}

auto primeTest(int n){
    for(int a = 2; a <= sqrt(n); a++){
        if (n % a == 0){
            return false;
        }
    }
    return true;
}

bool validation(int &n, int &a){
    if(n > 0 && primeTest(n) && a > 0 && a < n) return true;
    return false;
}

int gropeBase(int &n, int &a){
    int p = n - 1;
    vector<int> Q;
    for(int i = 2; i < n / 2 + 1; ++i){
        if (primeTest(i) && p % i == 0){
            while (p % i == 0) p /= i;
            Q.push_back(i);
        }
    }
    for (int j = 2; j < n; ++j){
        bool k = true;
        for (int i : Q){
            int b = pow(j, (n-1) / i, n);
            if (b % n == 1){
                k = false;
                break;
            }
        }
        if (k) return j;
    }
    return -1;
}

void PEcreate(int n, vector <int> &P, vector <int> &E){
    for (int i = 2; i <= n; ++i){
        if(primeTest(i) && n % i == 0){
            P.push_back(i);
            int e = 0;
            while (n % i == 0 ){
                e += 1;
                n /= i;
            }
            E.push_back(e);
        }
    }
}

int getX0(int a, int n, int p, vector<int> &r){
    int b = pow(a, (n-1) / p, n);
    for (int i = 0; i < r.size(); ++i) if (r[i] == b){
        return i;
    }
}

int getXj(int a, int n, int p, vector<int> &r, int g, int y, int k){
    int b = pow(a * pow(g, -y, n), (n-1)/pow(p, k+1, n), n);
    for (int i = 0; i < r.size(); ++i) if (r[i] == b) return i;
}

int KTO(vector<int> &p, vector<int> &e, vector<int> &y, int n){
    int res = 0;
    for(int i = 0; i < p.size(); i++){
        int m = pow(p[i], e[i], n);
        int M = (n-1)/m;
        int Ms = pow(M, -1, m);
        res += y[i] * M * Ms % (n-1);
    }
    return res;
}

int polig(int &g, int &n, int &a){
    vector <int> P, E, A, Y;
    PEcreate(n-1, P, E);
    vector <vector <int>> R(P.size(), vector<int>(0));
    for(int i = 0; i < P.size(); ++i){
        A.push_back(pow(g, (n-1)/P[i], n));
        for (int j = 0; j < P[i]; ++j){
            int l = pow(A[i], j, n);
            R[i].push_back(l);
        }
    }
    for(int i = 0; i < P.size(); ++i){
        int y = 0;
        int x0 = getX0(a, n, P[i], R[i]);
        y += x0;
        for(int j = 1; j < P.size(); ++j){
            int xj = getXj(a, n, P[i], R[i], g, y, j);
            y += xj * (int)pow(P[i], j) ;
        }
        Y.push_back(y);
    }
    int res = KTO(P, E, Y, n) % (n-1);
    return res;
}

int getRes(string &dir){
    ifstream file(dir);
    string inputString;
    regex patternString("[0-9]+ [0-9]+");
    string numberString, borderString;
    if(file.is_open()){
        ofstream out;
        out.open("output.txt", ios::app);
        while(getline(file, inputString)) {
            int n = 0, a = 0;
            if (regex_match(inputString, patternString)) {
                getNumbers(inputString, n, a);
                if(validation(n, a)){
                    int g = gropeBase(n, a);
                    if (g != -1){
                        int res = polig(g, n, a);
                        if(res != -1){
                            out << res << "\n";
                        }
                    }
                } else continue;
            }
        }
        file.close();
        out.close();
        return 1;
    }
    else return 0;
}

int main() {
    string directory = "input.txt";
    int res = getRes(directory);
    return res;
}
