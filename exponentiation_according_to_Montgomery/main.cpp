
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <cmath>

using namespace std;

unsigned long long int numbers[4] {0,0,0,0};
std::string one_str_from_file;
std::string numbers_str[4] {"", "", "", ""};
std::regex pattern_str("[0-9]+ [0-9]+ [0-9]+ [0-9]+");

void get_numbers() {
    int index = 0;
    for(char i : one_str_from_file){
        if(i != ' ') numbers_str[index] += i;
        if(i == ' ') index += 1;
    }
    for(char i : numbers_str[0]) numbers[0] = numbers[0] * 10 + i - 48;
    for(char i : numbers_str[1]) numbers[1] = numbers[1] * 10 + i - 48;
    for(char i : numbers_str[2]) numbers[2] = numbers[2] * 10 + i - 48;
    for(char i : numbers_str[3]) numbers[3] = numbers[3] * 10 + i - 48;
}

unsigned long long MP(unsigned long long x, unsigned long long y, unsigned long long m, int power, int k) {
    int b = pow(2, power), mask, newM = 1;
    mask = b - 1;
    while ((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = 0, xi, u;
    for (int i = 0u; i < k; i++) {
        xi = (x >> power * i) & mask;
        u = ((z & mask) + xi * (y & mask)) * newM % b;
        z = (z + xi * y + u * m) / b;
    }
    if (z > m) z -= m;
    return z;
}

unsigned long long MR(unsigned long long x, unsigned long long m, int deg, int k) {
    int b = pow(2, deg);
    int mask = b - 1;
    int newM = 1;
    while ((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = x;
    for (int i = 0; i < k; i++) {
        unsigned long long zi = (z >> deg * i) & mask;
        int u = (zi * newM) % b;
        z = z + u * m * pow(b, i);
    }
    z = z / (int)(pow(b, k));
    if (z >= m) z -= m;
    return z;
}

unsigned long long ME(unsigned long long x, unsigned long long y, unsigned long long m, unsigned long long b) {
    unsigned int deg = 1, k = 0, n = 0, R2D2 = 1, newX, z;
    while (pow(2, deg) != b) deg++;
    //    cout << "hey beach, look at me!\n";
    while ((m >> deg * k) != 0) k++;
    while ((y >> n) != 0) n++;
    for (int i = 0; i < 2 * k; i++) R2D2 = R2D2 * b % m;
    newX = MP(x, R2D2, m, deg, k);
    z = newX;
    for (int i = n - 2; i >= 0; i--) {
        z = MP(z, z, m, deg, k);
        if (((y >> i) & 1) == 1) z = MP(z, newX, m, deg, k);
    }
    return MR(z, m, deg, k);
}

void out_res(unsigned long long int result){
    std::ofstream out;
    out.open("output.txt", std::ios::app);
    out << result << "\n";
}

void check_numbers(){
    if(numbers[1] >= 1 && numbers[0] < numbers[2]){
        try{
            unsigned long long int b = numbers[3];
            int bits = 0;
            while (b != 0) {
                bits += (b & 1u);
                b >>= 1u;
            }
            if (bits != 1) throw std::exception();
            unsigned long long int result = ME(numbers[0], numbers[1], numbers[2], numbers[3]);
            out_res(result);
            return;
        }
        catch (std::exception& e) {}
    }
}

int numb_define(std::string dir){
    std::ifstream file(dir);
    if(file.is_open()){
        while(getline(file, one_str_from_file)) {
            if (std::regex_match(one_str_from_file, pattern_str)) {
                for(int i = 0; i < 4; i++){
                    numbers[i] = 0;
                    numbers_str[i] = "";
                }
                get_numbers();
                check_numbers();
            }
        }
        return 1;
    }
    else return 0;
}

int main(){
    std::string directory = "input.txt";
    int res = numb_define(directory);
    return res;
}
